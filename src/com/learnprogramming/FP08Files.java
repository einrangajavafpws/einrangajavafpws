package com.learnprogramming;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/*
lesson 41 section 8.
working with files:
Files.lines(), map(), flatMap(), distinct(), sorted().
Files.list(), filter().
 */
public class FP08Files {

    public static void main(String[] args) throws IOException {
        //Files.lines(Paths.get("file.txt")).forEach(System.out::println);

        /*task:
        find the unique words in that file.
        */
        /*Files.lines(Paths.get("file.txt"))
                .map(str -> str.split("")) //creates three String array with individual characters.
                .flatMap(Arrays::stream) //creates a stream with the String array elements.
                .forEach(System.out::println);*/

        /*Files.lines(Paths.get("file.txt"))
                .map(str -> str.split(" ")) //creates three String array with words.
                .flatMap(Arrays::stream) //creates a stream with the String array elements.
                .distinct() //filters the duplicated words.
                .sorted()
                .forEach(System.out::println);*/

        /*task:
        list all files and directories for this project at the root level.
         */
        //Files.list(Paths.get(".")).forEach(System.out::println);

        /*task:
        list all directories for this project at the root level.
         */
        Files.list(Paths.get("."))
                .filter(Files::isDirectory)
                .forEach(System.out::println);
    }
}
