package com.learnprogramming;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
lesson 36-37 section 7.
collect(Collectors.joining(), split(), flatMap(),
distinct().
high order functions.
 */
public class FP07Functional {
    public static void main(String[] args) {
        List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices",
                "AWS", "PCF", "Azure", "Docker", "Kubernetes");

        //task: create a list separating elements by " * ".
        /*System.out.println(
            courses.stream().collect(Collectors.joining(" * ")));*/

        //task: create a list with the characters of each stream element.
        /*System.out.println(
                courses.stream()
                        .map(course -> course.split(""))
                        .flatMap(Arrays::stream)
                        .collect(Collectors.toList()));*/

        //task: create a list with the distintc characters of each stream element.
        /*System.out.println(
                courses.stream()
                        .map(course -> course.split(""))
                        .flatMap(Arrays::stream)
                        .distinct()
                        .collect(Collectors.toList()));*/

        //task: having to lists, create a list containing all tuples pf the two lists.
        List<String> courses2 = List.of("Spring", "Spring Boot", "API", "Microservices",
                "AWS", "PCF", "Azure", "Docker", "Kubernetes");
        System.out.println(
                courses.stream()
                        .flatMap(course -> courses2.stream()
                        .map(course2 -> List.of(course, course2)))
                        .collect(Collectors.toList()));

        //task: based on the previous result, remove the duplicate tuples.
        /*System.out.println(
                courses.stream()
                        .flatMap(course -> courses2.stream()
                                .map(course2 -> List.of(course, course2)))
                        .filter(list -> !list.get(0).equals(list.get(1)))
                        .collect(Collectors.toList()));*/

        //task: based on the previous result, remove the same length tuples.
        /*System.out.println(
                courses.stream()
                        .flatMap(course -> courses2.stream()
                                .filter(course2 -> course2.length() != course.length())
                                .map(course2 -> List.of(course, course2)))
                        .filter(list -> !list.get(0).equals(list.get(1)))
                        .collect(Collectors.toList()));*/
    }

    Predicate<Course> reviewScoreGreaterThan95Predicate =
            createPredicateWithCutoffWithReviewScore(95);

    Predicate<Course> reviewScoreGreaterThan90Predicate =
            createPredicateWithCutoffWithReviewScore(90);

    /*
    this is a high order function.
    a high order function returns another function.
     */
    private static Predicate<Course> createPredicateWithCutoffWithReviewScore(int cutoffReviewScore2) {
        return course -> course.getReviewScore() > cutoffReviewScore2;
    }
}
