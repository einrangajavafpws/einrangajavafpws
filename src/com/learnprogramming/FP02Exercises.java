package com.learnprogramming;

import java.util.List;

/*
   lesson 12 section 3.
   multiple exercises:
   7. square every number in a list and find the sum of squares.
   8. cube every number in a list and find the sum of cubes.
   9. find sum of odd  numbers in a list.
 */
public class FP02Exercises {
	final static String SPRING = "Spring";
	
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

		//System.out.println(printSumOfSquareOfNumbers(numbers));
		//System.out.println(printSumOfCubesOfNumbers(numbers));
		System.out.println(printSumOfOddNumbersOfNumbers(numbers));
	}

	private static int printSumOfSquareOfNumbers(List<Integer> numbers) {
		//what to do?
		return numbers.stream()
				.map(x -> x * x)
				.reduce(0, Integer::sum);
	}

	private static int printSumOfCubesOfNumbers(List<Integer> numbers) {
		return numbers.stream()
				.map(x -> x * x * x)
				.reduce(0, Integer::sum);
	}

	private static int printSumOfOddNumbersOfNumbers(List<Integer> numbers) {
		return numbers.stream()
				.filter(x -> x % 2 != 0)
				.reduce(0, Integer::sum);
	}
}
