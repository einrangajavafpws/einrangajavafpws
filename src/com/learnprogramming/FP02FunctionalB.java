package com.learnprogramming;

import java.util.List;
/*
lesson 11 section 3.
get the sum of all elements of a collection
using the fp approach.
it works with reduce() and lambda.
*/

public class FP02FunctionalB {
	public static void main(String[] args) {
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		int sum = addListFunctional(numbers);
		System.out.println(sum);
	}

	private static int addListFunctional(List<Integer> numbers) {
		return numbers.stream()
				.reduce(0, (x, y) -> x + y);
	}
}
