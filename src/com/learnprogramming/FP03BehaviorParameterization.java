package com.learnprogramming;

import java.util.List;
/*
lesson 20 section 4.
print the even and odd numbers
of the collection.
phase 1 approach.
 */
public class FP03BehaviorParameterization {

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        numbers.stream()
                .filter(x -> x % 2 == 0)
                .forEach(System.out::println);

        numbers.stream()
                .filter(x -> x % 2 != 0)
                .forEach(System.out::println);
    }
}
