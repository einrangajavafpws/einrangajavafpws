package com.learnprogramming;

import java.util.List;

/*
lesson 38 section 7.
Performance. findFirst(), peek().
 */
public class FP07FunctionalB {
    public static void main(String[] args) {
        List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices",
                "AWS", "PCF", "Azure", "Docker", "Kubernetes");

        /*System.out.println(
                courses.stream()
                        .filter(course -> course.length() > 11 )
                        .map(String::toUpperCase)
                        .findFirst());*/

        //same exercise by parts:
        courses.stream()
                .peek(System.out::println)
                .filter(course -> course.length() > 11 )
                .map(String::toUpperCase)
                .peek(System.out::println)
                .findFirst();
        /*
        fp is about performance. in the previous example it doesn't take the whole elements.
        As soon a match to filter is found, it stops he process ignoring the last elements.
         */
    }
}
