package com.learnprogramming;

import java.util.List;
import java.util.function.Predicate;

/*
lesson 20 section 4.
print the even and odd numbers
of the collection.
phase 2 approach:
extract the filter arguments as variables.
 */
public class FP03BehaviorParameterizationB {

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        Predicate<Integer> evenPredicate = x -> x % 2 == 0;
        numbers.stream()
                .filter(evenPredicate)
                .forEach(System.out::println);

        Predicate<Integer> oddPredicate = x -> x % 2 != 0;
        numbers.stream()
                .filter(oddPredicate)
                .forEach(System.out::println);
    }
}
