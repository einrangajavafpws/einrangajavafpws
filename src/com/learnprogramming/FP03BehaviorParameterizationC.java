package com.learnprogramming;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/*
lesson 20 section 4.
print the even and odd numbers
of the collection.
phase 3 approach:
convert the statements applied to the collection
to print either the even numbers or the odd ones
from the collection using the extract method option.
this will create only one method accepting the
filter result as a parameter (predicate).
 */
public class FP03BehaviorParameterizationC {

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        Predicate<Integer> evenPredicate = x -> x % 2 == 0;
        filterAndPrint(numbers, evenPredicate);

        Predicate<Integer> oddPredicate = x -> x % 2 != 0;
        filterAndPrint(numbers, oddPredicate);
    }

    private static void filterAndPrint(List<Integer> numbers, Predicate<Integer> predicate) {
        numbers.stream()
                .filter(predicate)
                .forEach(System.out::println);
    }
}
