package com.learnprogramming;

import java.util.List;
/*
lesson 10 section 3.
get the sum of all elements of a collection
using the fp approach.
it works with reduce().
*/

public class FP02Functional {
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		int sum = addListFunctional(numbers);

		System.out.println(sum);
	}

	private static int addListFunctional(List<Integer> numbers) {
		return numbers.stream()
				.reduce(0, FP02Functional::sum);
		/*
		the first argument of reduce() is the initial value,
		the second one is the functional method.
		 */
	}

	private static int sum(int a, int b) {
		return a + b;
	}
}
