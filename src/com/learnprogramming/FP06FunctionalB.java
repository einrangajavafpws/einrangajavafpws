package com.learnprogramming;

import java.math.BigInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/*
lesson 34-35 section 6.
using a generator (or algorithm to create a stream).
IntStream.range(), IntStream.range(), IntStream.iterate(),
limit(), peek(), boxed(), collect(Collectors.toList()),
LongStream(), mapToObj(BigInteger::valueOf),
reduce(BigInteger.ONE, BigInteger::multiply).
 */
public class FP06FunctionalB {
    public static void main(String[] args) {
        /*System.out.println(
                IntStream.range(1, 10));

        System.out.println(
                IntStream.range(1, 10).sum()); //the last element isn't included in the sum.
        System.out.println(
                IntStream.rangeClosed(1, 10).sum()); //the last element is included in the sum.

        IntStream.iterate(1, num -> num + 2).limit(10).peek(System.out::println).sum();
        System.out.println(
            IntStream.iterate(1, num -> num + 2).limit(10).sum());*/

        /*IntStream.iterate(2, num -> num + 2).limit(10).peek(System.out::println).sum();
        IntStream.range(1, 100).filter(num -> num % 2 == 0).limit(10).peek(System.out::println).sum();*/

        //list the first 10 squares of 2
        //IntStream.iterate(2, num -> num * 2).limit(10).peek(System.out::println).sum();

        /*convert the previous result in a list.
        boxed() is requiered because you can't apply collect() to a stream of
        primitives.
         */
        /*System.out.println(
            IntStream.iterate(2, num -> num * 2).limit(10).boxed().collect(Collectors.toList()));
*/
        //calculate factorial of 50.
        System.out.println(
            LongStream.rangeClosed(1, 50)
                    .mapToObj(BigInteger::valueOf)
                    .reduce(BigInteger.ONE, BigInteger::multiply));
    }
}
