package com.learnprogramming;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/*
lesson 33 section 6.
more ways to create streams directly.
count(), reduce(), average(), min(0, and max().
 */
public class FP06Functional {
    @SuppressWarnings("unused")
    public static void main(String[] args) {
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        numbers.stream();
        /*System.out.println(numbers);

        System.out.println(
            Stream.of(12, 9, 13, 4, 6, 2, 4, 12, 15).count());

        System.out.println(
                Stream.of(12, 9, 13, 4, 6, 2, 4, 12, 15).reduce(0, Integer::sum));*/

        int[] numberArray = {12, 9, 13, 4, 6, 2, 4, 12, 15};
        //this will build a stream of primitives
        Arrays.stream(numberArray);

        //these are more efficient because box/unbox is unnecessary.
        System.out.println(
                Arrays.stream(numberArray).sum());
        System.out.println(
                Arrays.stream(numberArray).average());
        System.out.println(
                Arrays.stream(numberArray).min());
        System.out.println(
                Arrays.stream(numberArray).max());
    }
}
