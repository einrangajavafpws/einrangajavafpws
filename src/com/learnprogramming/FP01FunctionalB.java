package com.learnprogramming;

import java.util.List;

/*
   lesson 5 section 2.
   improve the method reference.
   it uses the System.out class because
   it includes the println(). 
 */
public class FP01FunctionalB {
	public static void main(String[] args) {
		
		printAllNumbersInListFunctional(List.of(12, 9, 13, 4, 6, 2, 4, 12, 15));
	}

	private static void printAllNumbersInListFunctional(List<Integer> numbers) {
		//what to do?
		/*
		 * the goal is to convert that list of numbers
		 * into a stream of numbers.
		 */
		numbers.stream()
			.forEach(System.out :: println); //method reference
	}
}
