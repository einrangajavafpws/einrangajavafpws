package com.learnprogramming;

import java.util.List;

/*
   lesson 7 section 2.
   multiple exercises:
   a. print only odd numbers.
   b. print all courses individually.
   c. print courses containing the word "Spring".
   d. print courses whose name has at least 4 characters.
 */
public class FP01Exercises {
    final static String SPRING = "Spring";

    public static void main(String[] args) {

        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
        List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices",
                "AWS", "PCF", "Azure", "Docker", "Kubernetes");
        //printOddNumbersInListFunctional(numbers);
        //printAllCourses(courses);
        //printSpringCourses(courses);
        printFourCharCourses(courses);
    }

    private static void printOddNumbersInListFunctional(List<Integer> numbers) {
        //what to do?
        numbers.stream()
                .filter(number -> number % 2 != 0) //filter: only allows odd numbers.
                .forEach(System.out::println); //method reference
    }

    private static void printAllCourses(List<String> courses) {
        //what to do?
        courses.stream()
                .forEach(System.out::println); //method reference
    }

    private static void printSpringCourses(List<String> courses) {
        //what to do?
        courses.stream()
                .filter(course -> course.contains(SPRING))
                .forEach(System.out::println); //method reference
    }

    private static void printFourCharCourses(List<String> courses) {
        //what to do?
        courses.stream()
                .filter(course -> course.length() >= 4)
                .forEach(System.out::println); //method reference
    }
}
