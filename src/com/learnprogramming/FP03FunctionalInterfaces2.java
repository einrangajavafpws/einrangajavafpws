package com.learnprogramming;

import java.util.List;
import java.util.Random;
import java.util.function.*;

/*
lesson 22, 23 section 4.
UnaryOperator, BiPredicate, BiFunction, BiConsumer,
and primitive functional.
 */
public class FP03FunctionalInterfaces2 {

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        //Predicate represents a boolean condition.
        Predicate<Integer> isEvenPredicate = x -> x % 2 == 0;
        //with Function there is an input and an output.
        Function<Integer, Integer> squareFunction = x -> x * x;
        Function<Integer, String> stringOuputFunction = x -> x + " ";

        //Consumer takes an input but it returns nothing.
        Consumer<Integer> sysoutConsumer = System.out::println;

        /*with Operator implies that all the inputs and outputs are of the
        same type. it takes 2 parameters as input.
         */
        BinaryOperator<Integer> sumBinaryOperator = (x, y) -> x + y;

        //with Supplier there isn't input and returns something.
        Supplier<Integer> integerSupplier = () -> 2;
        //example
        //System.out.println(integerSupplier.get());
        //another example
        Supplier<Integer> RandomIntegerSupplier = () -> {
            Random random = new Random();
            return random.nextInt(1000);
        };
        //System.out.println(RandomIntegerSupplier.get());

        /* with UnaryOperator, it takes 1 parameter as input.
        it returns the result of the same type as the ouput.

         */
        UnaryOperator<Integer> unaryOperator = (x) -> 3 * x;
        //ex:
        //System.out.println(unaryOperator.apply(5));

        /*in BiPredicate there are 2 input parameters
        and it returns a boolean.
         */
        BiPredicate<Integer, String> biPredicate = (number, str) -> {
            return number < 10 & str.length() > 5;
        };
        //ex:
        //System.out.println(biPredicate.test(5, "in28Minutes"));

        //BiFunction takes two inputs and returns one output.
        BiFunction<Integer, String, String> biFunction = (number, str) -> {
            return number + " " + str;
        };
        //ex:
        //System.out.println(biFunction.apply(15, "in28Minutes"));

        //BiConsumer takes two inputs and returns nothing.
        BiConsumer<Integer, String > biConsumer = (s1, s2) -> {
            //System.out.println(s1);
            //System.out.println(s2);
        };
        //ex:
        biConsumer.accept(15, "in28Minutes");

        /* these work with primitives.
        there are also Long*, Double* and some conversions.
         */
        IntBinaryOperator intBinaryOperator = (x, y) -> x + y;
        //ex:
        System.out.println(intBinaryOperator.applyAsInt(4, 5));
        //IntConsumer
        //IntFunction
        //IntPredicate
        //IntSupplier
        //IntToDoubleFunction
        //IntToLongFunction
        //IntUnaryOperator

        /*numbers.stream()
                .filter(isEvenPredicate)
                .map(squareFunction)
                .forEach(sysoutConsumer);

        int sum = numbers.stream()
                .reduce(0, sumBinaryOperator);*/
    }
}
