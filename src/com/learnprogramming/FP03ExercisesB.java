package com.learnprogramming;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

/*
   lesson 21 section 4.
   multiple exercises:
   13. do behavior parameterization for the mapping logic.
   List squaredNumbers = numbers.stream.map(x -> X*X).collect(Collectors.toList());
 */
public class FP03ExercisesB {
	
	public static void main(String[] args) {

		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

		/* initial code
		List<Integer> squaredNumbers = numbers.stream()
										.map(x -> x * x)
										.collect(Collectors.toList());
		*/

		/* phase 1: extract arguments as a var.
		Function<Integer, Integer> mappingFunction = x -> x * x;
		List<Integer> squaredNumbers = numbers.stream()
				.map(mappingFunction)
				.collect(Collectors.toList());*/

		/* phase 2: convert the statements applied to the collection
					to map a function using the extract method option.
		Function<Integer, Integer> mappingFunction = x -> x * x;
		List<Integer> squaredNumbers = mapAndCreateList(numbers, mappingFunction);
	}

	private static List<Integer> mapAndCreateList(List<Integer> numbers, Function<Integer, Integer> mappingFunction) {
		return numbers.stream()
				.map(mappingFunction)
				.collect(Collectors.toList());
	*/

		/*
		phase 3 convert the mappingFunction function in an inline variable.
		this allows to expand the mapAndCreateList() functionality.
		 */
		List<Integer> squaredNumbers = mapAndCreateList(numbers, x -> x * x);

		List<Integer> cubedNumbers = mapAndCreateList(numbers, x -> x * x * x);

		List<Integer> doubledNumbers = mapAndCreateList(numbers, x -> x + x);

		System.out.println(doubledNumbers);
	}

	private static List<Integer> mapAndCreateList(List<Integer> numbers, Function<Integer, Integer> mappingFunction) {
		return numbers.stream()
				.map(mappingFunction)
				.collect(Collectors.toList());
	}
}
