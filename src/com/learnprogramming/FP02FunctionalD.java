package com.learnprogramming;

import java.util.List;
import java.util.stream.Collectors;
/*
lesson 16 section 3.
create a list based in another list.

*/

public class FP02FunctionalD {
	public static void main(String[] args) {
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		List<Integer> doubledNumbers = doubledList(numbers);
		System.out.println(doubledNumbers);
	}

	private static List<Integer> doubledList(List<Integer> numbers) {
		return numbers.stream()
				.map(number -> number * number)
				.collect(Collectors.toList());
	}
}
