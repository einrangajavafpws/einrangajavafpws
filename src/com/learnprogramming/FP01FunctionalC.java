package com.learnprogramming;

import java.util.List;

/*
   lesson 5 section 2.
   print only even numbers.
   Uses filter(); 
 */
public class FP01FunctionalC {
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		printEvenNumbersInListFunctional(numbers);
	}

	private static void printEvenNumbersInListFunctional(List<Integer> numbers) {
		//what to do?		
		numbers.stream()			
			.filter(FP01FunctionalC :: isEven) //filter: only allows even numbers.
			.forEach(System.out::println); //method reference
	}
	
	private static boolean isEven(int number) {
		return number % 2 == 0;
	}
}
