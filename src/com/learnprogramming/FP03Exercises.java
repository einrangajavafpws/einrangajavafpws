package com.learnprogramming;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

/*
   lesson 19 section 4.
   multiple exercises:
   12. find functional interface behind the second argument of the
   reduce(). create an implementation for the functional interface.
   int sum = numbers.stream().reduce(0, Integer::sum);
 */
public class FP03Exercises {
	
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		//int sum = numbers.stream().reduce(0, Integer::sum);
		//int sum = numbers.stream().reduce(0, (x, y) -> x + y);
		BinaryOperator<Integer> sum1 = Integer::sum;
		int sum = numbers.stream().reduce(0, sum1);
		System.out.println(sum);

		BinaryOperator<Integer> sum2 = new BinaryOperator<Integer>() {
			@Override
			public Integer apply(Integer x, Integer y) {
				return x + y;
			}
		};
	}
}
