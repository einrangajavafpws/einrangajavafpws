package com.learnprogramming;
/*
lesson 39 section 7.
Parallel processing.
parallel().

 */

import java.util.stream.LongStream;

public class FP07Parallelizing {
    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        //I will create a strem af numbers 0 -> 1000000000 and add them.
        System.out.println(
            //LongStream.range(0, 1000000000).sum());
            LongStream.range(0, 1000000000).parallel().sum());
        System.out.println(System.currentTimeMillis() - time);
    }
}
