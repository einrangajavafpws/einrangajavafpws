package com.learnprogramming;

import java.util.List;
import java.util.function.Predicate;

/*
lesson 20 section 4.
print the even and odd numbers
of the collection.
phase 4 approach:
convert the evenPredicate and oddPredicate
to an inline argument using the inline variable option.
using this flexibility prints the multiples of 3.
 */
public class FP03BehaviorParameterizationD {

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        filterAndPrint(numbers, x1 -> x1 % 2 == 0);

        filterAndPrint(numbers, x -> x % 2 != 0);

        filterAndPrint(numbers, x -> x % 3 == 0);
    }

    private static void filterAndPrint(List<Integer> numbers, Predicate<Integer> predicate) {
        numbers.stream()
                .filter(predicate)
                .forEach(System.out::println);
    }
}
