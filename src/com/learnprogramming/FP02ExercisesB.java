package com.learnprogramming;

import java.util.List;
import java.util.stream.Collectors;

/*
   lesson 16 section 3.
   multiple exercises:
   10. create a list with even numbers filtered from the number list.
   11. create a list with lengths of all course titles.
 */
public class FP02ExercisesB {
	
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices",
				"AWS", "PCF", "Azure", "Docker", "Kubernetes");

		List<Integer> evenNumbers = evenNumbersList(numbers);
		System.out.println(evenNumbers);

		List<Integer> stringLengths = stringLengthsList(courses);
		System.out.println(stringLengths);
	}

	private static List<Integer> evenNumbersList(List<Integer> numbers) {
		//what to do?
		return numbers.stream()
				.filter(number -> number % 2 == 0)
				.collect(Collectors.toList());
	}

	private static List<Integer> stringLengthsList(List<String> courses) {
		return courses.stream()
				.map(str -> str.length())
				.collect(Collectors.toList());
	}
}
