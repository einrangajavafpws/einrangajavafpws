package com.learnprogramming;

import java.util.List;
/*
lesson 11 section 3.
get the sum of all elements of a collection
using the fp approach.
it works with reduce() and an internal method reference.
sum() is a method of Integer class.
*/

public class FP02FunctionalC {
	public static void main(String[] args) {
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		int sum = addListFunctional(numbers);
		System.out.println(sum);
	}

	private static int addListFunctional(List<Integer> numbers) {
		return numbers.stream()
				.reduce(0, Integer::sum);
	}
}
