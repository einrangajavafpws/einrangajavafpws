package com.learnprogramming;

import java.util.List;
/*
lesson 3 section 2.
print a list of numbers working with 
structured approach rather than functional programming.
*/

public class FP01Structured {
	public static void main(String[] args) {
		
		printAllNumbersInListStructured(List.of(12, 9, 13, 4, 6, 2, 4, 12, 15));
	}

	private static void printAllNumbersInListStructured(List<Integer> numbers) {		
		//how to loop the numbers?
		for (int number : numbers) {
			System.out.println(number);
		}
	}

}
