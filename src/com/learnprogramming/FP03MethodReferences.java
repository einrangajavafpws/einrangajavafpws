package com.learnprogramming;

import java.util.List;
import java.util.function.Supplier;

/*
lesson 25 section 4.
using method reference.
static method reference.
constructor reference.
 */
public class FP03MethodReferences {

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices",
                "AWS", "PCF", "Azure", "Docker", "Kubernetes");

        courses.stream()
                //.map(str -> str.toUpperCase())
                .map(String::toUpperCase) //using method reference.
                //.forEach(str -> System.out.println(str));
                //.forEach(System.out::println); //using method reference.
                //.forEach(System.out::print); //using method reference.
                .forEach(FP03MethodReferences::print); //using method reference.
    }

    //Supplier<String> supplier = () -> new String();
    Supplier<String> supplier = String::new; //using constructor reference.

    private static void print(String str) {
        System.out.println(str);
    }
}
