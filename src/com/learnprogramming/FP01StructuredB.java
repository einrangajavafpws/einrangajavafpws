package com.learnprogramming;

import java.util.List;
/*
lesson 5 section 2.
print a list of the even numbers only, working with 
structured approach rather than functional programming.
*/

public class FP01StructuredB {
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		printEvenNumbersInListStructured(numbers);
	}

	private static void printEvenNumbersInListStructured(List<Integer> numbers) {		
		//how to loop the numbers?
		for (int number : numbers) {
			if (number % 2 == 0) {
				System.out.println(number);
			}			
		}
	}

}
