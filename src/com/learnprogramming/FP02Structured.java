package com.learnprogramming;

import java.util.List;
/*
lesson 10 section 3.
get the sum of all elements of a collection
using the traditional structured approach.
*/

public class FP02Structured {
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		int sum = addListStructured(numbers);

		System.out.println(sum);
	}

	private static int addListStructured(List<Integer> numbers) {
		//how to store the summ?
		//how to loop?
		int sum = 0;
		for (int number:numbers){
			sum += number;
		}
		return sum;
	}
}
