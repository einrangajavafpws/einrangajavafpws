package com.learnprogramming;

import java.util.List;

/*
   lesson 4 section 2.
   print a list of numbers working with 
   functional programming approach.
   starts using streams and then method reference.
 */
public class FP01Functional {
	public static void main(String[] args) {
		
		printAllNumbersInListFunctional(List.of(12, 9, 13, 4, 6, 2, 4, 12, 15));
	}

	private static void printAllNumbersInListFunctional(List<Integer> numbers) {
		//what to do?
		/*
		 * the goal is to convert that list of numbers
		 * into a stream of numbers.
		 */
		numbers.stream()
			.forEach(FP01Functional::print); //method reference
	}
	
	private static void print(int number) {
		System.out.println(number);
	}
}
