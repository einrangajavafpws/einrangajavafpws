package com.learnprogramming;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
/*
lesson 17 section 3.
when the return is an stream it's considered an
intermediate operations.
when the returns is an specific type or not returning any,
it's considered a terminal operation.
 */
public class FP02StreamOperations {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        numbers.stream()
                .distinct()  //Stream<T>
                .sorted()   //Stream<T>
                .forEach(System.out::println); //void

        List<Integer> squaredNumbers = numbers.stream()
                                        .map(number -> number * number) //Stream<T>
                                        .collect(Collectors.toList()); //R

        List<Integer> evenNumberOnly = numbers.stream()
                                        .filter(number -> number % 2 == 0) //Stream<T>
                                        .collect(Collectors.toList());

        int sum = numbers.stream()
                    .reduce(0, (x, y) -> x * x + y * y); //T

        int greatest = numbers.stream()
                .reduce(Integer.MIN_VALUE, (x, y) -> x > y ? x : y);

        List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices",
                "AWS", "PCF", "Azure", "Docker", "Kubernetes");

        List<String> coursesSortedByLenghtOfCourseTitle = courses.stream()
                                                            .sorted(Comparator.comparing(str -> str.length()))
                                                            .collect(Collectors.toList());
    }
}
