package com.learnprogramming;

import java.awt.*;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
lesson 26-29 section 5.
creating a custom class (Course).
allMatch(), noneMatch(), and anyMatch()
comparators.
limit(), skip(), takeWhile(), and dropWhile().
returning a single element. max(), min(),
orElse(), findFirst(), and findAny().
sum(), average(), and count().
groupingBy(), counting(), maxBy(), and mapping().
 */
class Course {
    private String name;
    private String category;
    private int reviewScore;
    private int noOfStudents;

    public Course(String name, String category, int reviewScore, int noOfStudents) {
        this.name = name;
        this.category = category;
        this.reviewScore = reviewScore;
        this.noOfStudents = noOfStudents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getReviewScore() {
        return reviewScore;
    }

    public void setReviewScore(int reviewScore) {
        this.reviewScore = reviewScore;
    }

    public int getNoOfStudents() {
        return noOfStudents;
    }

    public void setNoOfStudents(int noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    @Override
    public String toString() {
        return name + ":" + noOfStudents + ":" +  reviewScore;
    }
}
public class FP04CustomClass {
    public static void main(String[] args) {
        List<Course> courses = List.of(
                new Course("Spring", "Framework", 98, 20000),
                new Course("Spring Boot", "Framework", 95, 18000),
                new Course("API", "Microservices", 97, 22000),
                new Course("Microservices", "Microservices", 96, 25000),
                new Course("FullStack", "FullStack", 91, 14000),
                new Course("AWS", "Cloud", 92, 21000),
                new Course("Azure", "Cloud", 99, 21000),
                new Course("Docker", "Cloud", 92, 20000),
                new Course("Kubernetes", "Cloud", 91, 20000)
        );

        //goal: to check if all courses have reviews > 90.
        Predicate<Course> reviewScoreGreaterThan95Predicate =
                course -> course.getReviewScore() > 95;
        Predicate<Course> reviewScoreGreaterThan90Predicate =
                course -> course.getReviewScore() > 90;

        Predicate<Course> reviewScoreLessThan90Predicate =
                course -> course.getReviewScore() < 90;
        /*
        System.out.println(courses.stream()
                .allMatch(reviewScoreGreaterThan95Predicate));

        System.out.println(courses.stream()
                .allMatch(reviewScoreGreaterThan90Predicate));

        System.out.println(courses.stream()
                .noneMatch(reviewScoreGreaterThan95Predicate));

        System.out.println(courses.stream()
                .noneMatch(reviewScoreLessThan90Predicate));

        System.out.println(courses.stream()
                .anyMatch(reviewScoreLessThan90Predicate));

        System.out.println(courses.stream()
                .anyMatch(reviewScoreGreaterThan90Predicate));

         */

        Comparator<Course> comparingByNoOFStudentsIncreasing
                = Comparator.comparing(Course::getNoOfStudents);
        /*System.out.println(
                courses
                .stream()
                .sorted(comparingByNoOFStudentsIncreasing)
                .collect(Collectors.toList())
        );*/

        Comparator<Course> comparingByNoOFStudentsDecreasing
                = Comparator.comparing(Course::getNoOfStudents).reversed();
        /*System.out.println(
                courses
                .stream()
                .sorted(comparingByNoOFStudentsDecreasing)
                .collect(Collectors.toList())
        );*/
        /*
            comparingInt() is more efficient than
            using comparing() because boxing and unboxing.
         */
        Comparator<Course> comparingByNoOFStudentsAndNoOfReviews
                = Comparator.comparingInt(Course::getNoOfStudents)
                .thenComparingInt(Course::getReviewScore)
                .reversed();
        /*System.out.println(
                courses
                        .stream()
                        .sorted(comparingByNoOFStudentsAndNoOfReviews)
                        .collect(Collectors.toList())
        );*/

        //limiting the results.
        /*System.out.println(
                courses
                        .stream()
                        .sorted(comparingByNoOFStudentsAndNoOfReviews)
                        .limit(5)
                        .collect(Collectors.toList())
        );*/
        //skipping the first three.
        /*System.out.println(
                courses
                        .stream()
                        .sorted(comparingByNoOFStudentsAndNoOfReviews)
                        .skip(3)
                        .collect(Collectors.toList())
        );*/
        //skipping the first three and taking only 5 from the balance.
        /*System.out.println(
                courses
                        .stream()
                        .sorted(comparingByNoOFStudentsAndNoOfReviews)
                        .skip(3)
                        .limit(5)
                        .collect(Collectors.toList())
        );*/

        //System.out.println(courses);

        //pick courses until a condition is matched.
        /*System.out.println(
                courses
                        .stream()
                        .takeWhile(course -> course.getReviewScore() >= 95)
                        .collect(Collectors.toList())
        );*/

        //drop courses if a condition is matched.
        /*System.out.println(
                courses
                        .stream()
                        .dropWhile(course -> course.getReviewScore() >= 95)
                        .collect(Collectors.toList())
        );*/

        /*System.out.println(
                courses.stream()
                        .max(comparingByNoOFStudentsAndNoOfReviews)
        );

        System.out.println(
                courses.stream()
                        .min(comparingByNoOFStudentsAndNoOfReviews)
        );

        System.out.println(
                courses.stream()
                        .filter(reviewScoreLessThan90Predicate)
                        .min(comparingByNoOFStudentsAndNoOfReviews)
        );

        System.out.println(
                courses.stream()
                        .filter(reviewScoreLessThan90Predicate)
                        .min(comparingByNoOFStudentsAndNoOfReviews)
                        .orElse(new Course("Kubernets", "Cloud", 91, 20000))
        );

        System.out.println(
                courses.stream()
                        .filter(reviewScoreLessThan90Predicate)
                        .findFirst()
        );

        System.out.println(
                courses.stream()
                        .filter(reviewScoreGreaterThan95Predicate)
                        .findFirst()
        );

        System.out.println(
                courses.stream()
                        .filter(reviewScoreGreaterThan95Predicate)
                        .findAny()
        );

        System.out.println(
                courses.stream()
                        .filter(reviewScoreGreaterThan95Predicate)
                        .mapToInt(Course::getNoOfStudents)
                        .sum()
        );

        System.out.println(
                courses.stream()
                        .filter(reviewScoreGreaterThan95Predicate)
                        .mapToInt(Course::getNoOfStudents)
                        .average()
        );

        System.out.println(
                courses.stream()
                        .filter(reviewScoreGreaterThan95Predicate)
                        .mapToInt(Course::getNoOfStudents)
                        .count()
        );*/

        //grouping
        System.out.println(
                courses.stream()
                        .collect(Collectors.groupingBy(Course::getCategory))
        );

        System.out.println(
                courses.stream()
                        .collect(Collectors.groupingBy(Course::getCategory, Collectors.counting()))
        );

        System.out.println(
                courses.stream()
                        .collect(Collectors.groupingBy(Course::getCategory,
                                Collectors.maxBy(Comparator.comparing(Course::getReviewScore))))
        );

        System.out.println(
                courses.stream()
                        .collect(Collectors.groupingBy(Course::getCategory,
                                Collectors.mapping(Course::getName, Collectors.toList())))
        );
    }
}