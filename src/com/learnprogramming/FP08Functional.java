package com.learnprogramming;

import java.util.ArrayList;
import java.util.List;

/*
lesson 40 section 8.
working with lists:
Files.lines()
 */
public class FP08Functional {
    public static void main(String[] args) {
        List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices",
                "AWS", "PCF", "Azure", "Docker", "Kubernetes");

        /*task: convert all elements to uppercase.
        since courses is immutable, you need to duplicate
        the list in a mutable list.
         */
        List<String> modifiableCourses = new ArrayList(courses);
        modifiableCourses.replaceAll(str -> str.toUpperCase());
        //System.out.println(modifiableCourses);

        //task: remove elements < 6 characters.
        modifiableCourses.removeIf(str -> str.length() < 6);
        System.out.println(modifiableCourses);
    }
}
