package com.learnprogramming;

import java.io.IOException;
import java.util.stream.IntStream;

/*
lesson 42 section 8.
working with threads:
Traditional way and Functional Programming (with loop) way.
Functional Programming without loop.
 */
public class FP08Threads {
    public static void main(String[] args) {
        /*Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int k = 0; k <1000; k++) {
                    System.out.println(
                            Thread.currentThread().getId() + ":" + k
                    );
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();

        Thread thread1 = new Thread(runnable);
        thread1.start();

        Thread thread2 = new Thread(runnable);
        thread2.start();*/

        //with a functional approach:
        /*Runnable runnable2 = () -> {
            for (int k = 0; k <1000; k++) {
                System.out.println(
                        Thread.currentThread().getId() + ":" + k
                );
            }
        };

        Thread threadA = new Thread(runnable2);
        thread.start();

        Thread threadB = new Thread(runnable2);
        thread1.start();

        Thread threadC = new Thread(runnable2);
        thread2.start();*/

        //with a functional approach without the loop:
        Runnable runnable3 = () ->
                IntStream.range(0, 1000)
                        .forEach(k -> System.out.println(Thread.currentThread().getId() + ":" + k));

        Thread threadX = new Thread(runnable3);
        threadX.start();

        Thread threadY = new Thread(runnable3);
        threadY.start();

        Thread threadZ = new Thread(runnable3);
        threadZ.start();
    }
}
